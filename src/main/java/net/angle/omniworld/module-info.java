
module omni.world {
    requires static lombok;
    requires devil.util;
    requires java.logging;
    requires omni.network;
    requires omni.registry;
    requires omni.module;
    requires omni.generation;
    requires omni.serialization;
    exports net.angle.omniworld.api;
    exports net.angle.omniworld.api.space;
    exports net.angle.omniworld.api.chunks;
    exports net.angle.omniworld.api.chunks.datagrams;
    exports net.angle.omniworld.impl;
    exports net.angle.omniworld.impl.space;
    exports net.angle.omniworld.impl.chunks;
    exports net.angle.omniworld.impl.chunks.datagrams;
}