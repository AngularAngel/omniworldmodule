/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.omniworld.api.space;

import com.samrj.devil.math.Vec3i;

/**
 *
 * @author angle
 */
public interface BoundedVoxelSpace {
    
    /**
     * @return the number of voxels in an edge (length, width, or height) for this voxel container.
     */
    public int getEdgeLength();
    
    public default boolean containsCoordinates(Vec3i coords) {
        return containsCoordinates(coords.x, coords.y, coords.z);
    }
    
    public default boolean containsCoordinates(int x, int y, int z) {
        int edgeLength = getEdgeLength();
        return x >= 0 && x < edgeLength && y >= 0 && y < edgeLength &&
            z >= 0 && z < edgeLength;
    }
    
    public default void forEachWithin(int border, VoxelOperation operation) {
        int edgeLength = getEdgeLength();
        for (int x = -border; x < edgeLength + border; x++)
            for (int y = -border; y < edgeLength + border; y++)
                for (int z = -border; z < edgeLength + border; z++) {
                    operation.operate(x, y, z);
                }
    }
    
    public default void forEach(VoxelOperation operation) {
        forEachWithin(0, operation);
    }
    
    @FunctionalInterface
    public static interface VoxelOperation {
    
        public default void operate(Vec3i coords) {
            operate(coords.x, coords.y, coords.z);
        }

        public void operate(int x, int y, int z);
    }
}