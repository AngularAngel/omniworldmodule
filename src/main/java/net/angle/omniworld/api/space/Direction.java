package net.angle.omniworld.api.space;

import net.angle.omniworld.impl.space.PrimaryDirections;
import com.samrj.devil.math.Vec3;
import com.samrj.devil.math.Vec3i;
import net.angle.omniregistry.api.Datum;

/**
 *
 * @author angle
 */
public interface Direction {
    public String getName();
    public Axis getAxis();
    public Alignment getAlignment();
    public PrimaryDirections getOpposite();
    
    public default Vec3 step(Vec3 coord, int distance) {
        return getAxis().addToVec(coord, getAlignment().getStep() * distance);
    }
    
    public default Vec3 step(Vec3 coord) {
        return step(coord, 1);
    }
    
    public default Vec3i step(Vec3i coord, int distance) {
        return getAxis().addToVec(coord, getAlignment().getStep() * distance);
    }
    
    public default Vec3i step(Vec3i coord) {
        return step(coord, 1);
    }
    
    public default Vec3 getVec3() {
        return step(new Vec3());
    }
    
    public default Vec3i getVec3i() {
        return step(new Vec3i());
    }
    
    public default Vec3 setOnVec(Vec3 vec, float value) {
        return getAxis().setOnVec(vec, value * getAlignment().getStep());
    }
    
    public default Vec3i modifyStartingPosition(Vec3i pos) {
        return getAxis().addToVec(pos, getAlignment().getStartMod());
    }
    
    public default Vec3 modifyStartingPosition(Vec3 pos) {
        return modifyStartingPosition(pos, 1);
    }
    
    public default Vec3 modifyStartingPosition(Vec3 pos, float multiplier) {
        return getAxis().addToVec(pos, getAlignment().getStartMod() * multiplier);
    }
    
    public default boolean continueStepping(Vec3i coord, int limit) {
        return getAlignment().continueStepping(getAxis().getValue(coord), limit);
    }
}