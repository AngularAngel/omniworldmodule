package net.angle.omniworld.api.space;

import com.samrj.devil.math.Vec3;
import com.samrj.devil.math.Vec3i;

/**
 *
 * @author angle
 */
public interface CoordinateSystem {
    public int getBlockEdgeLengthOfChunk();
    public float getRealEdgeLengthOfBlock();
    
    public int getChunkCoordFromWorldCoord(int worldCoord);
    public int getVoxelCoordFromWorldCoord(int worldCoord);
    public int getWorldCoordFromRealCoord(float realCoord);
    
    public default  int getChunkCoordFromRealCoord(float realCoord) {
        return getChunkCoordFromWorldCoord(getWorldCoordFromRealCoord(realCoord));
    }
    
    public default int[] getChunkCoordsFromWorldCoords(int worldx, int worldy, int worldz) {
        int chunkx = getChunkCoordFromWorldCoord(worldx);
        int chunky = getChunkCoordFromWorldCoord(worldy);
        int chunkz = getChunkCoordFromWorldCoord(worldz);
        
        return new int[] {chunkx, chunky, chunkz};
    }
    
    public default Vec3i getChunkCoordsFromWorldCoords(Vec3i worldVec) {
        int chunkx = getChunkCoordFromWorldCoord(worldVec.x);
        int chunky = getChunkCoordFromWorldCoord(worldVec.y);
        int chunkz = getChunkCoordFromWorldCoord(worldVec.z);
        
        return new Vec3i(chunkx, chunky, chunkz);
    }
    
    public default int[] getVoxelCoordsFromWorldCoords(int worldx, int worldy, int worldz) {
        int voxelx = getVoxelCoordFromWorldCoord(worldx);
        int voxely = getVoxelCoordFromWorldCoord(worldy);
        int voxelz = getVoxelCoordFromWorldCoord(worldz);
        
        return new int[] {voxelx, voxely, voxelz};
    }
    
    public default Vec3i getVoxelCoordsFromWorldCoords(Vec3i worldVec) {
        int voxelx = getVoxelCoordFromWorldCoord(worldVec.x);
        int voxely = getVoxelCoordFromWorldCoord(worldVec.y);
        int voxelz = getVoxelCoordFromWorldCoord(worldVec.z);
        
        return new Vec3i(voxelx, voxely, voxelz);
    }
    
    public default int[] getChunkAndVoxelCoordsFromWorldCoords(int x, int y, int z) {
        int[] chunkCoords = getChunkCoordsFromWorldCoords(x, y, z);
        int[] voxelCoords = getVoxelCoordsFromWorldCoords(x, y, z);
        
        return new int[] {chunkCoords[0], chunkCoords[1], chunkCoords[2], voxelCoords[0], voxelCoords[1], voxelCoords[2]};
    }
    public default int[] getChunkAndVoxelCoordsFromWorldCoords(Vec3i worldCoords) {
        return this.getChunkAndVoxelCoordsFromWorldCoords(worldCoords.x, worldCoords.y, worldCoords.z);
    }
    
    public default Vec3i getWorldCoordsFromRealCoords(Vec3 realCoords) {
        return new Vec3i(getWorldCoordFromRealCoord(realCoords.x),
                         getWorldCoordFromRealCoord(realCoords.y),
                         getWorldCoordFromRealCoord(realCoords.z));
    }
    
    public default int[] getChunkAndVoxelCoordsFromRealCoords(Vec3 realCoords) {
        return getChunkAndVoxelCoordsFromWorldCoords(getWorldCoordsFromRealCoords(realCoords));
    }
    
    public default int[] getChunkCoordsFromRealCoords(float realx, float realy, float realz) {
        int chunkx = getChunkCoordFromRealCoord(realx);
        int chunky = getChunkCoordFromRealCoord(realy);
        int chunkz = getChunkCoordFromRealCoord(realz);
        
        return new int[] {chunkx, chunky, chunkz};
    }
    
    public default Vec3i getChunkCoordsFromRealCoords(Vec3 realvec) {
        int chunkx = getChunkCoordFromRealCoord(realvec.x);
        int chunky = getChunkCoordFromRealCoord(realvec.y);
        int chunkz = getChunkCoordFromRealCoord(realvec.z);
        
        return new Vec3i(chunkx, chunky, chunkz);
    }
    
    public default Vec3 getRealCoordsFromWorldCoords(Vec3i worldCoords) {
        return new Vec3(worldCoords).mult(getRealEdgeLengthOfBlock());
    }
}