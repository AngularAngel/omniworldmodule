package net.angle.omniworld.api.space;

import com.samrj.devil.math.Vec3;
import com.samrj.devil.math.Vec3i;

/**
 *
 * @author angle
 */
public interface Axis {
    public String getName();
    public boolean isVertical();
    public boolean isHorizontal();
    public Vec3 setOnVec(Vec3 vec, float value);
    public Vec3 addToVec(Vec3 vec, int value);
    public Vec3i addToVec(Vec3i vec, int value);
    public Vec3 addToVec(Vec3 vec, float value);
    public Vec3i addToVec(Vec3i vec, float value);
    public int getValue(int x, int y, int z);
        
    public default int getValue(Vec3i vec) {
        return getValue(vec.x, vec.y, vec.z);
    }
}