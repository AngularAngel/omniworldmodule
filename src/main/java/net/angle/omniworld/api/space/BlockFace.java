package net.angle.omniworld.api.space;

import com.samrj.devil.math.Vec2;
import com.samrj.devil.math.Vec3;
import com.samrj.devil.math.Vec3i;
import java.util.function.BiConsumer;

/**
 *
 * @author angle
 */
public interface BlockFace {
    public String getName();
    public int getId();
    public Direction getPosition();
    public Direction getAcross();
    public Direction getDown();
    public BlockFace getOpposite();
        
    public default Vec3 getNormal() {
        return getPosition().getVec3();
    }
        
    public default Vec3 getUpwards() {
        return getDown().getOpposite().getVec3();
    }

    public default Vec3 getDownwards() {
        return getDown().getVec3();
    }

    public default Vec3 getLeftwards() {
        return getAcross().getOpposite().getVec3();
    }

    public default Vec3 getRightwards() {
        return getAcross().getVec3();
    }

    public default Vec3 getInwards() {
        return getPosition().getOpposite().getVec3();
    }

    public default Vec3 getOutwards() {
        return getPosition().getVec3();
    }
        
    public default Vec3i getStartingPosition() {
        Vec3i pos = new Vec3i();

        getPosition().getOpposite().modifyStartingPosition(pos);

        getAcross().modifyStartingPosition(pos);
        getDown().modifyStartingPosition(pos);

        return pos;
    }
        
    public default Vec3 modifyDrawStart(Vec3 coord) {
        getPosition().getOpposite().modifyStartingPosition(coord);
        getAcross().modifyStartingPosition(coord);
        getDown().modifyStartingPosition(coord);

        return coord;
    }
        
    public default Vec3 modifyDrawStart(Vec3 coord, float multiplier) {
        getPosition().getOpposite().modifyStartingPosition(coord, multiplier);
        getAcross().modifyStartingPosition(coord, multiplier);
        getDown().modifyStartingPosition(coord, multiplier);

        return coord;
    }
    
    public default Vec3 getInwardsDrawStart(Vec3 coord) {
        getPosition().getOpposite().modifyStartingPosition(coord);
        getAcross().getOpposite().modifyStartingPosition(coord);
        getDown().modifyStartingPosition(coord);
        return coord;
    }
    
    public default Vec3 orientFaceInwards(Vec2 dimensions) {
        Vec3 orientation = new Vec3();

        getAcross().getOpposite().setOnVec(orientation, dimensions.x);
        getDown().setOnVec(orientation, dimensions.y);

        return orientation;
    }
        
    public default Vec3i moveAcross(Vec3i coord, int distance) {
        return getAcross().step(coord, distance);
    }

    public default Vec3i moveDown(Vec3i coord, int distance) {
        return getDown().step(coord, distance);
    }

    public default Vec3i moveIn(Vec3i coord, int distance) {
        return getPosition().getOpposite().step(coord, distance);
    }

    public default Vec3i moveAcross(Vec3i coord) {
        return getAcross().step(coord);
    }

    public default Vec3i moveDown(Vec3i coord) {
        return getDown().step(coord);
    }

    public default Vec3i moveIn(Vec3i coord) {
        return getPosition().getOpposite().step(coord);
    }
    
    public default boolean continueAcross(Vec3i coord, int limit) {
        return getAcross().continueStepping(coord, limit);
    }

    public default boolean continueDown(Vec3i coord, int limit) {
        return getDown().continueStepping(coord, limit);
    }

    public default boolean continueIn(Vec3i coord, int limit) {
        return getPosition().getOpposite().continueStepping(coord, limit);
    }
        
    public default void iterateOver(int edgeLength, BiConsumer<Integer, Integer> action) {
        for (int i = 0; i < edgeLength; i++) {
            for (int j = 0; j < edgeLength; j++) {
                action.accept(i, j);
            }
        }
    }
}