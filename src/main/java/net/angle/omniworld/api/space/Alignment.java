package net.angle.omniworld.api.space;

/**
 *
 * @author angle
 */
public interface Alignment {
    public String getName();
    public Alignment getOpposite();
    public int getStep();
    public boolean continueStepping(int value, int limit);
    public default int getStartMod() {
        return Math.max(0, -getStep());
    }
}