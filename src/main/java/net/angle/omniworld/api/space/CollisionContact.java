package net.angle.omniworld.api.space;

import com.samrj.devil.math.Vec3;

/**
 *
 * @author angle
 */
public interface CollisionContact extends Comparable<CollisionContact> {
    public Vec3 getPosition();
    public Vec3 getNormal();
    public float getTime();

    @Override
    public default int compareTo(CollisionContact other) {
        return (int) (Float.compare(getTime(), other.getTime()));
    }
}
