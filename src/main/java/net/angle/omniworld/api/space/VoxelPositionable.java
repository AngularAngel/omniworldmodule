package net.angle.omniworld.api.space;

import com.samrj.devil.math.Vec3i;

/**
 *
 * @author angle
 */
public interface VoxelPositionable {
    public Vec3i getCoordinates();
    
    public default boolean hasSamePosition(VoxelPositionable other) {
        return getCoordinates().equals(other.getCoordinates());
    }
    
    public default boolean hasPosition(Vec3i other) {
        return getCoordinates().equals(other);
    }
    
    public default int axialDist(VoxelPositionable positionable) {
        return axialDist(positionable.getCoordinates());
    }
    
    public default int axialDist(Vec3i otherCoords) {
        Vec3i thisCoords = getCoordinates();
        return Math.max(Math.abs(otherCoords.x - thisCoords.x), Math.max(Math.abs(otherCoords.y - thisCoords.y), Math.abs(otherCoords.z - thisCoords.z)));
    }
}
