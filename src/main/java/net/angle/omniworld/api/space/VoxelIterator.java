package net.angle.omniworld.api.space;

import com.samrj.devil.math.Vec3i;

/**
 *
 * @author angle
 */
public interface VoxelIterator {
    public void iterate(Vec3i center);
    public void iterate(Vec3i center, BoundedVoxelSpace.VoxelOperation operation);
}