package net.angle.omniworld.api;

import net.angle.omnimodule.api.OmniModule;
import net.angle.omniregistry.api.RegistryCollection;
import net.angle.omniregistry.impl.BasicDatumRegistry;
import net.angle.omniserialization.api.ObjectSerializerRegistry;

/**
 *
 * @author angle
 */
public interface OmniWorldModule extends OmniModule {
    public default void populateRegistries(RegistryCollection registries) {
        registries.addEntry(new BasicDatumRegistry("Chunk Components", registries));
    }
    
    public void prepSerializerRegistry(ObjectSerializerRegistry serializerRegistry);
}