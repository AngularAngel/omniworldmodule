package net.angle.omniworld.api.chunks.datagrams;

import com.samrj.devil.math.Vec3i;
import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omniworld.api.chunks.VoxelComponent;

/**
 *
 * @author angle
 */
public interface SetArrayLongDatagramAction extends SetArrayVoxelsDatagramAction<long[][][]> {

    @Override
    public default void execute(Chunk chunk) {
        VoxelComponent<Long> component = (VoxelComponent<Long>) chunk.getChunkMap().getChunkComponents().get(getComponentID());
        Vec3i dimensions = getDimensions();
        Vec3i startCoords = getStartCoords();
        long[][][] values = getValues();
        for (int i = 0; i < dimensions.x; i++)
            for (int j = 0; j < dimensions.y; j++)
                for (int k = 0; k < dimensions.z; k++)
                    chunk.set(component, startCoords.x + i, startCoords.y + j, startCoords.z + k, values[i][j][k]);
    }
}