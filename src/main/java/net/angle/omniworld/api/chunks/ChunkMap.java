package net.angle.omniworld.api.chunks;

import net.angle.omniworld.api.space.CoordinateSystem;
import com.samrj.devil.math.Vec3i;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import net.angle.omniworld.api.space.BoundedVoxelSpace.VoxelOperation;

/**
 *
 * @author angle
 */
public interface ChunkMap extends VoxelContainer {
    public CoordinateSystem getCoordinateSystem();
    public List<VoxelComponent> getChunkComponents();
    
    public Chunk addChunk(Chunk chunk);
    public void chunkUpdated(Chunk chunk);
    public void removeChunk(Chunk chunk);
    public void removeChunk(Vec3i chunkCoords);
    
    public default boolean hasChunkAt(Vec3i chunkCoords) {
        return Objects.nonNull(getChunk(chunkCoords));
    }
    
    public Chunk getChunk(Vec3i chunkCoords);
    public default Chunk getChunk(int chunkx, int chunky, int chunkz) {
        return getChunk(new Vec3i(chunkx, chunky, chunkz));
    }
    
    public int[] getChunkAndVoxelCoords(int worldx, int worldy, int worldz);
    
    public void addChunkAdditionCallback(Consumer<Chunk> callback);
    public void addChunkUpdateCallback(Consumer<Chunk> callback);
    public void addChunkRemovalCallback(Consumer<Chunk> callback);
    
    public void forEachChunk(Consumer<Chunk> operation);
    public default void forEachChunkWithin(Vec3i startCoords, Vec3i dimensions, VoxelOperation operation) {
        CoordinateSystem coordinateSystem = getCoordinateSystem();
        for (int i = 0; i < dimensions.x; i += coordinateSystem.getBlockEdgeLengthOfChunk())
            for (int j = 0; j < dimensions.y; j += coordinateSystem.getBlockEdgeLengthOfChunk())
                for (int k = 0; k < dimensions.z; k += coordinateSystem.getBlockEdgeLengthOfChunk())
                    operation.operate(startCoords.x + i, startCoords.y + j, startCoords.z + k);
    }
    

    @Override
    public default <T> T get(int componentID, int x, int y, int z) {
        return get((VoxelComponent<T>) getChunkComponents().get(componentID), x, y, z);
    }

    @Override
    public default <T> void set(int componentID, int x, int y, int z, T obj) {
        set((VoxelComponent<T>) getChunkComponents().get(componentID), x, y, z, obj);
    }

    @Override
    public default <T> T update(int componentID, int x, int y, int z, T obj) {
        return update((VoxelComponent<T>) getChunkComponents().get(componentID), x, y, z, obj);
    }
    
    @FunctionalInterface
    public static interface ChunkConstructor<T extends Chunk> {
        public T generateChunk(ChunkMap map, int x, int y, int z);
        
        public default T generateChunk(ChunkMap map, Vec3i coordinates) {
            return generateChunk(map, coordinates.x, coordinates.y, coordinates.z);
        }
    }
}