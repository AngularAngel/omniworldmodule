package net.angle.omniworld.api.chunks;

import net.angle.omniworld.api.space.BoundedVoxelSpace;
import com.samrj.devil.math.Vec3i;

/**
 *
 * @author angle
 */
public interface BoundedVoxelContainer extends VoxelContainer, BoundedVoxelSpace {
    
    public <T> boolean isHomogenous(VoxelComponent<T> component);
    
    public <T> boolean isHomogenousDefault(VoxelComponent<T> component);
    
    public <T> void setAll(VoxelComponent<T> component, T obj);
    
    public <T> void updateAll(VoxelComponent<T> component, T obj);
    
    public boolean contains(Vec3i worldPosition);
}