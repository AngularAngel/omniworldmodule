package net.angle.omniworld.api.chunks.datagrams;

import net.angle.omninetwork.api.DatagramAction;
import net.angle.omniworld.api.chunks.Chunk;

/**
 *
 * @author angle
 */
public interface SetVoxelsDatagramAction extends DatagramAction<Chunk> {
    public int getComponentID();
}