package net.angle.omniworld.api.chunks.datagrams;

import java.util.Objects;
import net.angle.omninetwork.api.ActionList;
import net.angle.omniworld.api.GameWorld;
import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omniworld.api.chunks.ChunkMap;
import net.angle.omniworld.impl.chunks.BasicChunk;

/**
 *
 * @author angle
 */
public interface GetChunkDatagramAction extends ChunkDatagramAction, ActionList<Chunk> {

    @Override
    public default void execute(GameWorld gameWorld) {
        ChunkMap chunkMap = gameWorld.getChunkMap();
        Chunk chunk = chunkMap.getChunk(getCoordinates());
        if (Objects.isNull(chunk)) {
            chunk = new BasicChunk(chunkMap, getCoordinates());
            executeActions(chunk);
            gameWorld.getChunkMap().addChunk(chunk);
        } else {
            executeActions(chunk);
            chunkMap.chunkUpdated(chunk);
        }
    }
}