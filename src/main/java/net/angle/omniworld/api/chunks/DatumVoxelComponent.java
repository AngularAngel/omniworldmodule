package net.angle.omniworld.api.chunks;

import com.samrj.devil.math.Vec3i;
import net.angle.omninetwork.api.DatagramAction;
import net.angle.omniregistry.api.Datum;
import net.angle.omniregistry.api.Registry;
import net.angle.omniworld.api.space.CoordinateSystem;
import net.angle.omniworld.impl.chunks.datagrams.BasicSetArrayDatumDatagramAction;
import net.angle.omniworld.impl.chunks.datagrams.BasicSetHomogenousDatumDatagramAction;

/**
 *
 * @author angle
 */
public interface DatumVoxelComponent<T extends Datum> extends VoxelComponent<T> {
    public Registry<T> getComponentRegistry();

    @Override
    public default DatagramAction<Chunk> getSetActionForChunk(Chunk chunk) {
        if (chunk.isHomogenous(this)) {
            int datumID = chunk.get(this, 0, 0, 0).getId();
            if (datumID != 0)
                return new BasicSetHomogenousDatumDatagramAction(datumID, getId());
        } else {
            CoordinateSystem coordinateSystem = chunk.getChunkMap().getCoordinateSystem();
            int blockEdgeLengthOfChunk = coordinateSystem.getBlockEdgeLengthOfChunk();
            int[][][] datumIDs = new int[blockEdgeLengthOfChunk][blockEdgeLengthOfChunk][blockEdgeLengthOfChunk];
            chunk.forEach((x, y, z) -> {
                datumIDs[x][y][z] = chunk.get(this, x, y, z).getId();
            });

            return new BasicSetArrayDatumDatagramAction(new Vec3i(), new Vec3i(blockEdgeLengthOfChunk),
                        datumIDs, getId());
        }
        return null;
    }
}
