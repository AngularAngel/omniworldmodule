package net.angle.omniworld.api.chunks;

import net.angle.omniworld.api.space.BoundedVoxelSpace;
import com.samrj.devil.math.Vec3i;

/**
 *
 * @author angle
 */
public interface VoxelComponentContainer<T> extends BoundedVoxelSpace {
    public VoxelComponent<T> getComponent();
    public BoundedVoxelContainer getContainer();
    
    public default boolean isHomogenous() {
        return false;
    }
    
    public T get(int x, int y, int z);
    public void set(int x, int y, int z, T obj);
    public T update(int x, int y, int z, T obj);
    
    public default T get(Vec3i coord) {
        return get(coord.x, coord.y, coord.z);
    }
    
    public default void set(Vec3i coord, T obj) {
        set(coord.x, coord.y, coord.z, obj);
    }
    
    public default T update(Vec3i coord, T obj) {
        return update(coord.x, coord.y, coord.z, obj);
    }
    
    public default void setAll(T obj) {
        forEach((x, y, z) -> {
            set(x, y, z, obj);
        });
    }
    
    public default void updateAll(T obj) {
        forEach((x, y, z) -> {
            update(x, y, z, obj);
        });
    }
    
    @Override
    public default int getEdgeLength() {
        return getContainer().getEdgeLength();
    }
}