/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package net.angle.omniworld.api.chunks;

import com.samrj.devil.math.Vec3i;

/**
 *
 * @author angle
 */
public interface VoxelContainer {
    public <T> T get(VoxelComponent<T> component, int x, int y, int z);
    public <T> T get(int componentID, int x, int y, int z);
    public default <T> T get(VoxelComponent<T> component, Vec3i coord) {
        return get(component, coord.x, coord.y, coord.z);
    }
    
    public <T> void set(VoxelComponent<T> component, int x, int y, int z, T obj);
    public <T> void set(int componentID, int x, int y, int z, T obj);
    public default <T> void set(VoxelComponent<T> component, Vec3i coord, T obj) {
        set(component, coord.x, coord.y, coord.z, obj);
    }
    
    public <T> T update(VoxelComponent<T> component, int x, int y, int z, T obj);
    public <T> T update(int componentID, int x, int y, int z, T obj);
    public default <T> T update(VoxelComponent<T> component, Vec3i coord, T obj) {
        return update(component, coord.x, coord.y, coord.z, obj);
    }
}