package net.angle.omniworld.api.chunks.datagrams;

import com.samrj.devil.math.Vec3i;
import net.angle.omninetwork.api.DatagramAction;
import net.angle.omniworld.api.GameWorld;

/**
 *
 * @author angle
 */
public interface ChunkDatagramAction extends DatagramAction<GameWorld> {
    public Vec3i getCoordinates();
}