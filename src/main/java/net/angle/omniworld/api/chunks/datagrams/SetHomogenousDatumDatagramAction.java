package net.angle.omniworld.api.chunks.datagrams;

import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omniworld.api.chunks.DatumVoxelComponent;


/**
 *
 * @author angle
 */
public interface SetHomogenousDatumDatagramAction extends SetVoxelsDatagramAction {
    public int getDatumID();

    @Override
    public default void execute(Chunk chunk) {
        DatumVoxelComponent component = (DatumVoxelComponent) chunk.getChunkMap().getChunkComponents().get(getComponentID());
        chunk.setAll(component, component.getComponentRegistry().getEntryById(getDatumID()));
    }
}