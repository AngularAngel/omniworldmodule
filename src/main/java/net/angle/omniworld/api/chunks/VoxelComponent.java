package net.angle.omniworld.api.chunks;

import net.angle.omninetwork.api.DatagramAction;
import net.angle.omniregistry.api.Datum;

/**
 *
 * @author angle
 */
public interface VoxelComponent<T> extends Datum {
    public Class<T> getComponentClass();
    public T getDefaultObject();
    public T update(T prev, T next);
    public VoxelComponentContainer<T> getHomogenousComponent(BoundedVoxelContainer container, T obj);
    public VoxelComponentContainer<T> getArrayComponent(BoundedVoxelContainer container, T obj);
    public default VoxelComponentContainer<T> getHomogenousComponent(BoundedVoxelContainer container) {
        return getHomogenousComponent(container, getDefaultObject());
    }
    public default VoxelComponentContainer<T> getArrayComponent(BoundedVoxelContainer container) {
        return getArrayComponent(container, getDefaultObject());
    }
    public DatagramAction<Chunk> getSetActionForChunk(Chunk chunk);
}