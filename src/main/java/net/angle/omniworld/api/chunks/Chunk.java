package net.angle.omniworld.api.chunks;

import com.samrj.devil.math.Vec3i;
import net.angle.omniworld.api.space.VoxelPositionable;

/**
 *
 * @author angle
 */
public interface Chunk extends VoxelPositionable, BoundedVoxelContainer {
    public ChunkMap getChunkMap();
    public void setChunkMap(ChunkMap chunkMap);

    @Override
    public default <T> T get(int componentID, int x, int y, int z) {
        return get((VoxelComponent<T>) getChunkMap().getChunkComponents().get(componentID), x, y, z);
    }

    @Override
    public default <T> void set(int componentID, int x, int y, int z, T obj) {
        set((VoxelComponent<T>) getChunkMap().getChunkComponents().get(componentID), x, y, z, obj);
    }

    @Override
    public default <T> T update(int componentID, int x, int y, int z, T obj) {
        return update((VoxelComponent<T>) getChunkMap().getChunkComponents().get(componentID), x, y, z, obj);
    }
    
    public <T> VoxelComponentContainer<T> getComponent(VoxelComponent<T> component);
    public VoxelComponentContainer[] getComponents();
    
    public Chunk getChunkFor(int x, int y, int z);
    public default Chunk getChunkFor(Vec3i coords) {
        return getChunkFor(coords.x, coords.y, coords.z);
    }
}