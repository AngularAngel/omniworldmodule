package net.angle.omniworld.api.chunks.datagrams;

import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omniworld.api.chunks.VoxelComponent;

/**
 *
 * @author angle
 */
public interface SetHomogenousLongDatagramAction extends SetVoxelsDatagramAction {
    public long getValue();

    @Override
    public default void execute(Chunk chunk) {
        VoxelComponent<Long> component = (VoxelComponent<Long>) chunk.getChunkMap().getChunkComponents().get(getComponentID());
        chunk.setAll(component, getValue());
    }
}