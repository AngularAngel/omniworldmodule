package net.angle.omniworld.api.chunks;

import net.angle.omniregistry.api.DatumBuilder;

/**
 *
 * @author angle
 */
public interface VoxelComponentBuilder<E extends VoxelComponent<T>, T> extends DatumBuilder<E> {
    public void setComponentClass(Class<T> componentClass);
    public void setDefaultObject(T defaultObject);
}