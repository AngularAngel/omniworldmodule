package net.angle.omniworld.api.chunks.datagrams;

import com.samrj.devil.math.Vec3i;

/**
 *
 * @author angle
 */
public interface SetArrayVoxelsDatagramAction<T> extends SetVoxelsDatagramAction {
    public Vec3i getStartCoords();
    public Vec3i getDimensions();
    public T getValues();
}