package net.angle.omniworld.api.chunks.datagrams;

import com.samrj.devil.math.Vec3i;
import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omniworld.api.chunks.DatumVoxelComponent;

/**
 *
 * @author angle
 */
public interface SetArrayDatumDatagramAction extends SetArrayVoxelsDatagramAction<int[][][]> {

    @Override
    public default void execute(Chunk chunk) {
        DatumVoxelComponent component = (DatumVoxelComponent) chunk.getChunkMap().getChunkComponents().get(getComponentID());
        Vec3i dimensions = getDimensions();
        Vec3i startCoords = getStartCoords();
        int[][][] datumIDs = getValues();
        for (int i = 0; i < dimensions.x; i++)
            for (int j = 0; j < dimensions.y; j++)
                for (int k = 0; k < dimensions.z; k++)
                    chunk.set(component, startCoords.x + i, startCoords.y + j, startCoords.z + k,
                            component.getComponentRegistry().getEntryById(datumIDs[i][j][k]));
    }
}