package net.angle.omniworld.api;

import net.angle.omniworld.api.chunks.ChunkMap;
import com.samrj.devil.gl.ShaderProgram;
import com.samrj.devil.math.Vec3i;
import net.angle.omniregistry.api.RegistryCollection;
import net.angle.omniworld.api.chunks.Chunk;

/**
 *
 * @author angle
 */
public interface GameWorld {
    public RegistryCollection getRegistries();
    public ChunkMap getChunkMap();
    
    public Chunk getChunk(Vec3i coords);
    
    public void addChunk(Chunk chunk);
    
    public void init();
    public void step(float dt);
    public void destroy(Boolean crashed);
    
    public void prepShader(ShaderProgram shader);
}