package net.angle.omniworld.impl.chunks.datagrams;

import com.samrj.devil.math.Vec3i;
import net.angle.omniworld.api.chunks.datagrams.SetArrayDatumDatagramAction;

/**
 *
 * @author angle
 */
public class BasicSetArrayDatumDatagramAction extends AbstractSetArrayDatagramAction<int[][][]> implements SetArrayDatumDatagramAction {
    public BasicSetArrayDatumDatagramAction(Vec3i startCoords, Vec3i dimensions, int[][][] values, int componentID) {
        super(startCoords, dimensions, values, componentID);
    }
}
