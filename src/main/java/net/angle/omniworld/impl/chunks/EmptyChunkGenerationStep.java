package net.angle.omniworld.impl.chunks;

import com.samrj.devil.math.Vec3i;
import java.util.Objects;
import lombok.RequiredArgsConstructor;
import net.angle.omnigeneration.impl.SimpleGenerationStep;
import net.angle.omnigeneration.api.process.GenerationRequest;
import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omniworld.api.chunks.ChunkMap;
/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public class EmptyChunkGenerationStep extends SimpleGenerationStep<Chunk, Vec3i> {
    private final ChunkMap.ChunkConstructor<? extends Chunk> chunkConstructor;
    private final ChunkMap existingChunkMap;
    private final ChunkMap generatingChunkMap;
    
    @Override
    public void process(GenerationRequest<Chunk, Vec3i> request) {
        Chunk chunk = existingChunkMap.getChunk(request.getIdentifier());
        if (Objects.isNull(chunk))
            chunk = chunkConstructor.generateChunk(generatingChunkMap, request.getIdentifier());
        request.setResult(chunk);
    }
}