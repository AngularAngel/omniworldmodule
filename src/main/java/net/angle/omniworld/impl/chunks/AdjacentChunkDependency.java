package net.angle.omniworld.impl.chunks;

import com.samrj.devil.math.Vec3i;
import java.util.Objects;
import lombok.RequiredArgsConstructor;
import net.angle.omnigeneration.api.GenerationManager;
import net.angle.omnigeneration.api.process.GenerationRequest;
import net.angle.omnigeneration.api.process.GenerationStep;
import net.angle.omniworld.api.chunks.Chunk;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public class AdjacentChunkDependency implements GenerationStep.Dependency<Chunk, Vec3i> {
    private final Vec3i offset;
    private final GenerationStep<Chunk, Vec3i> step;
    private final GenerationManager<Chunk, Vec3i> generationManager;
    
    public Vec3i getCoords(Vec3i identifier) {
        return identifier.add(offset);
    }

    @Override
    public boolean isCompleted(GenerationRequest<Chunk, Vec3i> request) {
        Vec3i identifier = getCoords(request.getIdentifier());
        GenerationRequest<Chunk, Vec3i> otherRequest = generationManager.getRequest(identifier);
        return Objects.nonNull(otherRequest) && otherRequest.getCompletedSteps().contains(step);
    }
}