package net.angle.omniworld.impl.chunks.datagrams;

import lombok.Getter;
import lombok.ToString;
import net.angle.omniworld.api.chunks.datagrams.SetHomogenousDatumDatagramAction;

/**
 *
 * @author angle
 */
@ToString(callSuper = true)
public class BasicSetHomogenousDatumDatagramAction extends AbstractSetChunkVoxelsDatagramAction implements SetHomogenousDatumDatagramAction {
    private final @Getter int datumID;

    public BasicSetHomogenousDatumDatagramAction(int datumID, int componentID) {
        super(componentID);
        this.datumID = datumID;
    }
}