package net.angle.omniworld.impl.chunks.datagrams;

import com.samrj.devil.math.Vec3i;
import lombok.Getter;
import lombok.ToString;
import net.angle.omniworld.api.chunks.datagrams.ChunkDatagramAction;

/**
 *
 * @author angle
 */
@ToString(callSuper = true)
public abstract class AbstractChunkDatagramAction implements ChunkDatagramAction {
    private final @Getter Vec3i coordinates;

    public AbstractChunkDatagramAction(Vec3i coordinates) {
        this.coordinates = coordinates;
    }
}