package net.angle.omniworld.impl.chunks;

import com.samrj.devil.math.Vec3i;
import lombok.Getter;
import lombok.Setter;
import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omnigeneration.api.GenerationManager;
import net.angle.omnigeneration.impl.AbstractGenerationRequest;

/**
 *
 * @author angle
 */
public class BasicChunkGenerationRequest<T extends Chunk> extends AbstractGenerationRequest<T, Vec3i> {
    private final Vec3i identifier;
    private @Getter @Setter T result;

    public BasicChunkGenerationRequest(GenerationManager generationManager, Vec3i identifier) {
        super(generationManager);
        this.identifier = identifier;
    }

    public BasicChunkGenerationRequest(GenerationManager generationManager, int x, int y, int z) {
        this(generationManager, new Vec3i(x, y, z));
    }

    @Override
    public Vec3i getIdentifier() {
        return new Vec3i(identifier);
    }
}