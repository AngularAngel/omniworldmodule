package net.angle.omniworld.impl.chunks.datagrams;

import com.samrj.devil.math.Vec3i;
import net.angle.omniworld.api.chunks.datagrams.SetArrayLongDatagramAction;

/**
 *
 * @author angle
 */
public class BasicSetArrayLongDatagramAction extends AbstractSetArrayDatagramAction<long[][][]> implements SetArrayLongDatagramAction {

    public BasicSetArrayLongDatagramAction(Vec3i startCoords, Vec3i dimensions, long[][][] values, int componentID) {
        super(startCoords, dimensions, values, componentID);
    }
    
}