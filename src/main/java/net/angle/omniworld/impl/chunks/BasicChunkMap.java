/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.angle.omniworld.impl.chunks;

import com.samrj.devil.math.Vec3i;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omniworld.api.chunks.ChunkMap;
import net.angle.omniworld.api.space.CoordinateSystem;
import net.angle.omniworld.api.chunks.VoxelComponent;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public class BasicChunkMap implements ChunkMap {
    private final @Getter CoordinateSystem coordinateSystem;
    private final @Getter List<VoxelComponent> chunkComponents;
    
    private final List<Consumer<Chunk>> chunkAdditionCallbacks = Collections.synchronizedList(new ArrayList<>());
    private final List<Consumer<Chunk>> chunkUpdateCallbacks = Collections.synchronizedList(new ArrayList<>());
    private final List<Consumer<Chunk>> chunkRemovalCallbacks = Collections.synchronizedList(new ArrayList<>());
    
    //Stored by coordinates
    private final Map<Vec3i, Chunk> chunks = Collections.synchronizedMap(new HashMap<>());

    @Override
    public void forEachChunk(Consumer<Chunk> operation) {
        for (Chunk chunk: chunks.values().toArray(Chunk[]::new)) {
            operation.accept(chunk);
        }
    }
    
    @Override
    public void addChunkAdditionCallback(Consumer<Chunk> callback) {
        chunkAdditionCallbacks.add(callback);
    }
    
    @Override
    public void addChunkUpdateCallback(Consumer<Chunk> callback) {
        chunkUpdateCallbacks.add(callback);
    }
    
    @Override
    public void addChunkRemovalCallback(Consumer<Chunk> callback) {
        chunkRemovalCallbacks.add(callback);
    }
    
    @Override
    public void chunkUpdated(Chunk chunk) {
        for (Consumer<Chunk> callback: chunkUpdateCallbacks)
            callback.accept(chunk);
    }
    
    @Override
    public Chunk addChunk(Chunk chunk) {
        chunk.setChunkMap(this);
        chunks.put(chunk.getCoordinates(), chunk);
        for (Consumer<Chunk> callback: chunkAdditionCallbacks)
            callback.accept(chunk);
        return chunk;
    }
    
    @Override
    public void removeChunk(Chunk chunk) {
        if (Objects.isNull(chunk))
            return;
        
        chunks.remove(chunk.getCoordinates());
        for (Consumer<Chunk> callback: chunkRemovalCallbacks)
            callback.accept(chunk);
    }
    
    @Override
    public void removeChunk(Vec3i coordinates) {
        removeChunk(chunks.get(coordinates));
    }
    
    @Override
    public Chunk getChunk(int chunkx, int chunky, int chunkz) {
        return getChunk(new Vec3i(chunkx, chunky, chunkz));
    }
    
    @Override
    public Chunk getChunk(Vec3i chunkCoords) {
        if (chunkCoords == null)
            throw new NullPointerException();
        return chunks.get(chunkCoords);
    }
    
    @Override
    public int[] getChunkAndVoxelCoords(int worldx, int worldy, int worldz) {
        return coordinateSystem.getChunkAndVoxelCoordsFromWorldCoords(worldx, worldy, worldz);
    }
    
    @Override
    public <T> T get(VoxelComponent<T> component, int worldx, int worldy, int worldz) {
        int[] coords = getChunkAndVoxelCoords(worldx, worldy, worldz);
        
        Chunk chunk = getChunk(coords[0], coords[1], coords[2]);
        if (chunk == null) 
            return component.getDefaultObject();
        else
            return chunk.get(component, coords[3], coords[4], coords[5]);
    }
    
    @Override
    public <T> void set(VoxelComponent<T> component, int worldx, int worldy, int worldz, T value) {
        int[] coords = getChunkAndVoxelCoords(worldx, worldy, worldz);
        getChunk(coords[0], coords[1], coords[2]).set(component, coords[3], coords[4], coords[5], value);
    }
    
    @Override
    public <T> T update(VoxelComponent<T> component, int worldx, int worldy, int worldz, T value) {
        int[] coords = getChunkAndVoxelCoords(worldx, worldy, worldz);
        return getChunk(coords[0], coords[1], coords[2]).update(component, coords[3], coords[4], coords[5], value);
    }

    @Override
    public String toString() {
        return "ChunkMap: " + chunks.size() + " Chunks!";
    }
}