package net.angle.omniworld.impl.chunks;

import com.samrj.devil.math.Vec3i;
import lombok.RequiredArgsConstructor;
import net.angle.omnigeneration.api.process.GenerationRequest;
import net.angle.omnigeneration.impl.SimpleGenerationStep;
import net.angle.omniworld.api.GameWorld;
import net.angle.omniworld.api.chunks.Chunk;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public class FinalChunkGenerationStep<T extends Chunk> extends SimpleGenerationStep<T, Vec3i> {
    private final GameWorld gameWorld;

    @Override
    public void process(GenerationRequest<T, Vec3i> request) {
        gameWorld.addChunk(request.getResult());
    }
}