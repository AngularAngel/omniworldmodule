package net.angle.omniworld.impl.chunks;

import com.samrj.devil.math.Vec3i;
import java.util.Arrays;
import java.util.List;
import lombok.Getter;
import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omniworld.api.chunks.ChunkMap;
import net.angle.omniworld.api.chunks.VoxelComponent;
import net.angle.omniworld.api.chunks.VoxelComponentContainer;
import net.angle.omniworld.impl.space.BasicVoxelPositionable;

/**
 *
 * @author angle
 */
public class BasicChunk extends BasicVoxelPositionable implements Chunk {
    private @Getter ChunkMap chunkMap;
    private final VoxelComponentContainer[] components;
    
    public BasicChunk(ChunkMap chunkMap, int x, int y, int z) {
        super(x, y, z);
        this.chunkMap = chunkMap;
        this.components = new VoxelComponentContainer[chunkMap.getChunkComponents().size()];
        for (VoxelComponent component : (List<VoxelComponent>) chunkMap.getChunkComponents())
            setComponent(component, component.getHomogenousComponent(this));
    }
    
    public BasicChunk(ChunkMap chunkMap, Vec3i coordinates) {
        this(chunkMap, coordinates.x, coordinates.y, coordinates.z);
    }

    @Override
    public void setChunkMap(ChunkMap chunkMap) {
        if (this.chunkMap != chunkMap) {
            this.chunkMap.removeChunk((Chunk) this);
            this.chunkMap = chunkMap;
        }
    }
    
    @Override
    public <T> VoxelComponentContainer<T> getComponent(VoxelComponent<T> component) {
        return components[component.getId()];
    }
    
    private <T> VoxelComponentContainer<T> setComponent(VoxelComponent<T> component, VoxelComponentContainer<T> container) {
        return components[component.getId()] = container;
    }
    
    @Override
    public VoxelComponentContainer[] getComponents() {
        return Arrays.copyOf(components, components.length);
    }
    
    @Override
    public Chunk getChunkFor(int x, int y, int z) {
        return chunkMap.getChunk(chunkMap.getCoordinateSystem().getChunkCoordsFromWorldCoords(new Vec3i(x + this.x, y + this.y, z + this.z)));
    }
    
    @Override
    public <T> T get(VoxelComponent<T> component, int x, int y, int z) {
        if (!containsCoordinates(x, y, z))
            return chunkMap.get(component, x + this.x, y + this.y, z + this.z);
        return getComponent(component).get(x, y, z);
    }
    
    @Override
    public <T> void set(VoxelComponent<T> component, int x, int y, int z, T obj) {
        if (!containsCoordinates(x, y, z))
            chunkMap.set(component, x + this.x, y + this.y, z + this.z, obj);
        VoxelComponentContainer<T> containerComponent = getComponent(component);
        if (containerComponent.isHomogenous())
            containerComponent = setComponent(component, component.getArrayComponent(this, containerComponent.get(0, 0, 0)));
        containerComponent.set(x, y, z, obj);
    }
    
    @Override
    public <T> void setAll(VoxelComponent<T> component, T obj) {
        VoxelComponentContainer<T> containerComponent = getComponent(component);
        if (containerComponent.isHomogenous())
            containerComponent.setAll(obj);
        else
            setComponent(component, component.getHomogenousComponent(this, obj));
    }
    
    @Override
    public <T> T update(VoxelComponent<T> component, int x, int y, int z, T obj) {
        if (!containsCoordinates(x, y, z))
            return chunkMap.update(component, x + this.x, y + this.y, z + this.z, obj);
        VoxelComponentContainer<T> containerComponent = getComponent(component);
        if (containerComponent.isHomogenous())
            containerComponent = setComponent(component, component.getArrayComponent(this, containerComponent.get(0, 0, 0)));
        return containerComponent.update(x, y, z, obj);
    }
    
    @Override
    public <T> void updateAll(VoxelComponent<T> component, T obj) {
        getComponent(component).updateAll(obj);
    }
    
    @Override
    public <T> boolean isHomogenous(VoxelComponent<T> component) {
        return getComponent(component).isHomogenous();
    }
    
    @Override
    public <T> boolean isHomogenousDefault(VoxelComponent<T> component) {
        return getComponent(component).isHomogenous() && get(component, 0, 0, 0) == component.getDefaultObject();
    }
    
    @Override
    public boolean contains(Vec3i position) {
        return getCoordinates().equals(chunkMap.getCoordinateSystem().getChunkCoordsFromWorldCoords(position));
    }
    
    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (other instanceof BasicChunk) {
            return hasSamePosition((BasicVoxelPositionable) other);
        }
        return false;
    }
    
    @Override
    public int getEdgeLength() {
        return chunkMap.getCoordinateSystem().getBlockEdgeLengthOfChunk();
    }

    @Override
    public String toString() {
        return "Chunk(Coords: " + getCoordinates() + ", Components: " + components.length + ")";
    }
}