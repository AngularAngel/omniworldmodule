package net.angle.omniworld.impl.chunks;

import com.samrj.devil.math.zorder.ZOrderCurve;
import java.lang.reflect.Array;
import net.angle.omniworld.api.chunks.BoundedVoxelContainer;
import net.angle.omniworld.api.chunks.VoxelComponent;

/**
 *
 * @author angle
 */
public class ArrayChunkComponent<T> extends AbstractVoxelComponentContainer<T> {
    private final T[] array;

    public ArrayChunkComponent(BoundedVoxelContainer container, VoxelComponent<T> component, T obj) {
        super(container, component);
        int edgeLength = container.getEdgeLength();
        array = (T[]) Array.newInstance(component.getComponentClass(), edgeLength * edgeLength * edgeLength);
        setAll(obj);
    }
    
    public ArrayChunkComponent(BoundedVoxelContainer container, VoxelComponent<T> component) {
        this(container, component, component.getDefaultObject());
    }
    
    public T getUnchecked(int index) {
        return array[index];
    }
    
    public void setUnchecked(int index, T obj) {
        array[index] = obj;
    }

    @Override
    public T get(int x, int y, int z) {
        return getUnchecked(ZOrderCurve.encode3(x, y, z));
    }

    @Override
    public void set(int x, int y, int z, T obj) {
        setUnchecked(ZOrderCurve.encode3(x, y, z), obj);
    }

    @Override
    public T update(int x, int y, int z, T obj) {
        T result = getComponent().update(get(x, y, z), obj);
        set(x, y, z, result);
        return result;
    }
    
    @Override
    public void setAll(T obj) {
        for (int i = 0; i < array.length; i++)
            setUnchecked(i, obj);
    }
    
    @Override
    public String toString() {
        return "ArrayChunkComponent(Component: " + getComponent().getName() + ")";
    }
}