package net.angle.omniworld.impl.chunks;

import java.util.function.BiFunction;
import net.angle.omniregistry.api.Registry;
import net.angle.omniworld.api.chunks.VoxelComponent;

/**
 *
 * @author angle
 */
public abstract class UpdateFunctionVoxelComponent<T> extends AbstractVoxelComponent<T>{
    
    private final BiFunction<T, T, T> updateFunction;

    public UpdateFunctionVoxelComponent(String name, Registry<? extends VoxelComponent> registry, Class<T> componentClass, T defaultObject, BiFunction<T, T, T> updateFunction) {
        super(name, registry, componentClass, defaultObject);
        this.updateFunction = updateFunction;
    }

    @Override
    public T update(T prev, T next) {
        return updateFunction.apply(prev, next);
    }
}