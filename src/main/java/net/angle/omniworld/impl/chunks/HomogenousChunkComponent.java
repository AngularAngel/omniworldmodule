package net.angle.omniworld.impl.chunks;

import net.angle.omniworld.api.chunks.BoundedVoxelContainer;
import net.angle.omniworld.api.chunks.VoxelComponent;

/**
 *
 * @author angle
 */
public class HomogenousChunkComponent<T> extends AbstractVoxelComponentContainer<T> {
    private T obj;
    
    public HomogenousChunkComponent(BoundedVoxelContainer container, VoxelComponent<T> component, T obj) {
        super(container, component);
        this.obj = obj;
    }
    
    public HomogenousChunkComponent(BoundedVoxelContainer container, VoxelComponent<T> component) {
        this(container, component, component.getDefaultObject());
    }
    
    @Override
    public T get(int x, int y, int z) {
        if (!containsCoordinates(x, y, z))
            return getContainer().get(getComponent(), x, y, z);
        return obj;
    }
    
    @Override
    public void set(int x, int y, int z, T obj) {
        throw new UnsupportedOperationException("Not supported.");
    }
    
    @Override
    public T update(int x, int y, int z, T obj) {
        throw new UnsupportedOperationException("Not supported.");
    }
    
    @Override
    public void setAll(T obj) {
        this.obj = obj;
    }
    
    @Override
    public boolean isHomogenous() {
        return true;
    }
    
    @Override
    public void updateAll(T obj) {
        T result = getComponent().update(this.obj, obj);
        setAll(result);
    }
    
    @Override
    public String toString() {
        return "HomogenousChunkComponent(Component: " + getComponent().getName() + ", Value: " + obj + ")";
    }
}