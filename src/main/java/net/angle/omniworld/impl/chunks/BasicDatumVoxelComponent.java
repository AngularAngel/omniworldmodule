package net.angle.omniworld.impl.chunks;

import lombok.Getter;
import net.angle.omniregistry.api.Datum;
import net.angle.omniregistry.api.Registry;
import net.angle.omniworld.api.chunks.DatumVoxelComponent;
import net.angle.omniworld.api.chunks.VoxelComponent;

/**
 *
 * @author angle
 */
public class BasicDatumVoxelComponent<T extends Datum> extends AbstractVoxelComponent<T> implements DatumVoxelComponent<T> {
    public final @Getter Registry<T> componentRegistry;

    public BasicDatumVoxelComponent(String name, Registry<? extends VoxelComponent> registry, Class<T> componentClass, Registry<T> componentRegistry) {
        super(name, registry, componentClass, null);
        this.componentRegistry = componentRegistry;
    }

    @Override
    public T getDefaultObject() {
        return componentRegistry.getEntryById(0);
    }
    
    
}