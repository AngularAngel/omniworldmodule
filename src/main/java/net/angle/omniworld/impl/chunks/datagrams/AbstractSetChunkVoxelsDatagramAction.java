package net.angle.omniworld.impl.chunks.datagrams;

import lombok.Getter;
import lombok.ToString;
import net.angle.omniworld.api.chunks.datagrams.SetVoxelsDatagramAction;

/**
 *
 * @author angle
 */
@ToString(callSuper = true)
public abstract class AbstractSetChunkVoxelsDatagramAction implements SetVoxelsDatagramAction {
    private final @Getter int componentID;

    public AbstractSetChunkVoxelsDatagramAction(int componentID) {
        this.componentID = componentID;
    }
}