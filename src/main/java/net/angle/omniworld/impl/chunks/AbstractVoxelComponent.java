package net.angle.omniworld.impl.chunks;

import lombok.Getter;
import lombok.ToString;
import net.angle.omniregistry.api.Registry;
import net.angle.omniworld.api.chunks.VoxelComponent;
import net.angle.omniregistry.impl.AbstractDatum;
import net.angle.omniworld.api.chunks.BoundedVoxelContainer;
import net.angle.omniworld.api.chunks.VoxelComponentContainer;

/**
 *
 * @author angle
 */
@ToString
public abstract class AbstractVoxelComponent<T> extends AbstractDatum implements VoxelComponent<T> {
    private final @Getter Class<T> componentClass;
    private final @Getter T defaultObject;

    public AbstractVoxelComponent(String name, Registry<? extends VoxelComponent> registry, Class<T> componentClass, T defaultObject) {
        super(name, registry);
        this.componentClass = componentClass;
        this.defaultObject = defaultObject;
    }

    @Override
    public T update(T prev, T next) {
        return next;
    }

    @Override
    public VoxelComponentContainer<T> getHomogenousComponent(BoundedVoxelContainer container, T obj) {
        return new HomogenousChunkComponent<>(container, this, obj);
    }

    @Override
    public VoxelComponentContainer<T> getArrayComponent(BoundedVoxelContainer container, T obj) {
        return new ArrayChunkComponent<>(container, this, obj);
    }
}