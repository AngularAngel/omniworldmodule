package net.angle.omniworld.impl.chunks.datagrams;

import com.samrj.devil.math.Vec3i;
import lombok.Getter;
import net.angle.omninetwork.api.DatagramAction;
import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omniworld.api.chunks.datagrams.GetChunkDatagramAction;

/**
 *
 * @author angle
 */
public class BasicGetChunkDatagramAction extends AbstractChunkDatagramAction implements GetChunkDatagramAction {
    private final @Getter DatagramAction<Chunk>[] actions;

    public BasicGetChunkDatagramAction(DatagramAction<Chunk>[] actions, Vec3i coordinates) {
        super(coordinates);
        this.actions = actions;
    }
    
    public BasicGetChunkDatagramAction(DatagramAction<Chunk> action, Vec3i coordinates) {
        this(new DatagramAction[]{action}, coordinates);
    }
    
}