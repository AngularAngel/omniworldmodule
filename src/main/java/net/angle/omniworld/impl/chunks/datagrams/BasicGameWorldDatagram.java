package net.angle.omniworld.impl.chunks.datagrams;

import lombok.ToString;
import net.angle.omninetwork.api.DatagramAction;
import net.angle.omninetwork.impl.AbstractActionListDatagram;
import net.angle.omniworld.api.GameWorld;

/**
 *
 * @author angle
 */
@ToString(callSuper = true)
public class BasicGameWorldDatagram extends AbstractActionListDatagram<GameWorld> {
    public BasicGameWorldDatagram(DatagramAction<GameWorld>[] actions, int priority) {
        super(actions, priority);
    }
    public BasicGameWorldDatagram(DatagramAction<GameWorld> action, int priority) {
        super(new DatagramAction[]{action}, priority);
    }

    @Override
    public Class<GameWorld> getTargetClass() {
        return GameWorld.class;
    }
}