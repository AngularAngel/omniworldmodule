package net.angle.omniworld.impl.chunks.datagrams;

import lombok.Getter;
import net.angle.omniworld.api.chunks.datagrams.SetHomogenousLongDatagramAction;

/**
 *
 * @author angle
 */
public class BasicSetHomogenousLongDatagramAction extends AbstractSetChunkVoxelsDatagramAction implements SetHomogenousLongDatagramAction {
    private final @Getter long value;

    public BasicSetHomogenousLongDatagramAction(long value, int componentID) {
        super(componentID);
        this.value = value;
    }
}