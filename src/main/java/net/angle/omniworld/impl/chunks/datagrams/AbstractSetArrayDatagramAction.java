package net.angle.omniworld.impl.chunks.datagrams;

import com.samrj.devil.math.Vec3i;
import lombok.Getter;
import net.angle.omniworld.api.chunks.datagrams.SetArrayVoxelsDatagramAction;

/**
 *
 * @author angle
 */
public abstract class AbstractSetArrayDatagramAction<T> extends AbstractSetChunkVoxelsDatagramAction implements SetArrayVoxelsDatagramAction<T> {
    private final @Getter Vec3i startCoords;
    private final @Getter Vec3i dimensions;
    private final @Getter T values;

    public AbstractSetArrayDatagramAction(Vec3i startCoords, Vec3i dimensions, T values, int componentID) {
        super(componentID);
        this.startCoords = startCoords;
        this.dimensions = dimensions;
        this.values = values;
    }
}