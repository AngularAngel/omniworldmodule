package net.angle.omniworld.impl.chunks;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.angle.omniworld.api.chunks.BoundedVoxelContainer;
import net.angle.omniworld.api.chunks.VoxelComponentContainer;
import net.angle.omniworld.api.chunks.VoxelComponent;

/**
 *
 * @author angle
 */
@AllArgsConstructor
public abstract class AbstractVoxelComponentContainer<T> implements VoxelComponentContainer<T> {
    private final @Getter BoundedVoxelContainer container;
    private final @Getter VoxelComponent<T> component;
}