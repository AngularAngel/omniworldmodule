package net.angle.omniworld.impl;


import com.samrj.devil.math.Vec3i;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.angle.omnimodule.impl.BasicOmniModule;
import net.angle.omniserialization.api.ObjectSerializerRegistry;
import net.angle.omniworld.api.OmniWorldModule;
import net.angle.omniworld.impl.chunks.datagrams.BasicGameWorldDatagram;
import net.angle.omniworld.impl.chunks.datagrams.BasicSetHomogenousDatumDatagramAction;
import net.angle.omninetwork.api.DatagramAction;
import net.angle.omniworld.impl.chunks.datagrams.BasicGetChunkDatagramAction;
import net.angle.omniworld.impl.chunks.datagrams.BasicSetArrayDatumDatagramAction;
import net.angle.omniworld.impl.chunks.datagrams.BasicSetArrayLongDatagramAction;
import net.angle.omniworld.impl.chunks.datagrams.BasicSetHomogenousLongDatagramAction;


/**
 *
 * @author angle
 */
public class OmniWorldModuleImpl extends BasicOmniModule implements OmniWorldModule {
    public OmniWorldModuleImpl() {
        super(OmniWorldModule.class);
    }
    
    @Override
    public void prepSerializerRegistry(ObjectSerializerRegistry serializerRegistry) {
        try {
            serializerRegistry.registerObject(BasicGameWorldDatagram.class.getConstructor(DatagramAction[].class, int.class), (datagram) -> {
                return new Object[]{datagram.getActions(), datagram.getPriority()};
            });
            
            serializerRegistry.registerObject(BasicGetChunkDatagramAction.class.getConstructor(DatagramAction[].class, Vec3i.class), (t) -> {
                return new Object[]{t.getActions(), t.getCoordinates()};
            });
            
            serializerRegistry.registerObject(BasicSetHomogenousDatumDatagramAction.class.getConstructor(int.class, int.class), (t) -> {
                return new Object[]{t.getDatumID(), t.getComponentID()};
            });
            
            serializerRegistry.registerObject(BasicSetArrayDatumDatagramAction.class.getConstructor(Vec3i.class, Vec3i.class, int[][][].class, int.class), (t) -> {
                return new Object[]{t.getStartCoords(), t.getDimensions(), t.getValues(), t.getComponentID()};
            });
            
            serializerRegistry.registerObject(BasicSetHomogenousLongDatagramAction.class.getConstructor(long.class, int.class), (t) -> {
                return new Object[]{t.getValue(), t.getComponentID()};
            });
            
            serializerRegistry.registerObject(BasicSetArrayLongDatagramAction.class.getConstructor(Vec3i.class, Vec3i.class, long[][][].class, int.class), (t) -> {
                return new Object[]{t.getStartCoords(), t.getDimensions(), t.getValues(), t.getComponentID()};
            });
        } catch (NoSuchMethodException | SecurityException ex) {
            Logger.getLogger(OmniWorldModuleImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}