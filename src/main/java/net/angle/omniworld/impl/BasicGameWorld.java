package net.angle.omniworld.impl;

import com.samrj.devil.gl.ShaderProgram;
import com.samrj.devil.math.Vec3i;
import java.util.Objects;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import lombok.Getter;
import lombok.Setter;
import net.angle.omniregistry.api.RegistryCollection;
import net.angle.omniworld.api.chunks.ChunkMap;
import net.angle.omniworld.api.GameWorld;
import net.angle.omniworld.api.chunks.Chunk;

/**
 *
 * @author angle
 */
public class BasicGameWorld implements GameWorld {
    private @Getter @Setter RegistryCollection registries;
    private @Getter @Setter ChunkMap chunkMap;
    private final BlockingDeque<Chunk> chunksToAdd = new LinkedBlockingDeque<>();

    @Override
    public void init() {
        if (Objects.isNull(registries) || Objects.isNull(chunkMap))
            throw new IllegalStateException("Must set registries and chunkMap before initializing!");
    }

    @Override
    public void step(float dt) {
        while (!chunksToAdd.isEmpty()) {
            chunkMap.addChunk(chunksToAdd.poll());
        }
    }

    @Override
    public void prepShader(ShaderProgram shader) {
        getRegistries().prepShader(shader);
    }

    @Override
    public void destroy(Boolean crashed) {
        registries.destroy();
    }

    @Override
    public void addChunk(Chunk chunk) {
        chunksToAdd.add(chunk);
    }

    @Override
    public Chunk getChunk(Vec3i coords) {
        return chunkMap.getChunk(coords);
    }
}