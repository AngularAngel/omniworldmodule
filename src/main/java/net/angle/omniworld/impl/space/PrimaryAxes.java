package net.angle.omniworld.impl.space;

import com.samrj.devil.math.Vec3;
import com.samrj.devil.math.Vec3i;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.angle.omniworld.api.space.Axis;

/**
 *
 * @author angle
 */
@AllArgsConstructor
public enum PrimaryAxes implements Axis {
    X("X"),
    Y("Y"),
    Z("Z");

    public final @Getter String name;

    @Override
    public boolean isVertical() {
        return this == Y;
    }

    @Override
    public boolean isHorizontal() {
        return this != Y;
    }

    @Override
    public Vec3 setOnVec(Vec3 vec, float value) {
        switch(this) {
            case X -> vec.x += value;
            case Y -> vec.y += value;
            case Z -> vec.z += value;
            default -> throw new AssertionError(this.name());
        }
        return vec;
    }

    @Override
    public Vec3 addToVec(Vec3 vec, int value) {
        switch(this) {
            case X -> vec.x += value;
            case Y -> vec.y += value;
            case Z -> vec.z += value;
            default -> throw new AssertionError(this.name());
        }
        return vec;
    }
        
    @Override
    public Vec3i addToVec(Vec3i vec, int value) {
        switch(this) {
            case X:
                vec.x += value;
                return vec;
            case Y:
                vec.y += value;
                return vec;
            case Z:
                vec.z += value;
                return vec;
            default:
                throw new AssertionError(this.name());
        }
    }

    @Override
    public Vec3 addToVec(Vec3 vec, float value) {
        switch(this) {
            case X -> vec.x += value;
            case Y -> vec.y += value;
            case Z -> vec.z += value;
            default -> throw new AssertionError(this.name());
        }
        return vec;
    }
        
    @Override
    public Vec3i addToVec(Vec3i vec, float value) {
        switch(this) {
            case X:
                vec.x += value;
                return vec;
            case Y:
                vec.y += value;
                return vec;
            case Z:
                vec.z += value;
                return vec;
            default:
                throw new AssertionError(this.name());
        }
    }

    @Override
    public int getValue(int x, int y, int z) {
        switch(this) {
            case X: return x;
            case Y: return y;
            case Z: return z;
            default:
                throw new AssertionError(this.name());

        }
    }
}