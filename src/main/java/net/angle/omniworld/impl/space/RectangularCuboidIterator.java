package net.angle.omniworld.impl.space;

import com.samrj.devil.math.Vec3i;
import lombok.AllArgsConstructor;
import net.angle.omniworld.api.space.BoundedVoxelSpace;
import net.angle.omniworld.api.space.VoxelIterator;

/**
 *
 * @author angle
 */
@AllArgsConstructor
public class RectangularCuboidIterator implements VoxelIterator {
    private int edgeDistanceX;
    private int edgeDistanceY;
    private int edgeDistanceZ;
    private BoundedVoxelSpace.VoxelOperation operation;
    
    @Override
    public void iterate(Vec3i center) {
        iterate(center, operation);
    }
    
    @Override
    public void iterate(Vec3i center, BoundedVoxelSpace.VoxelOperation operation) {
        for (int i = -edgeDistanceX; i <= edgeDistanceX; i++)
            for (int j = -edgeDistanceY; j <= edgeDistanceY; j++)
                for (int k = -edgeDistanceZ; k <= edgeDistanceZ; k++)
                    operation.operate(center.x + i, center.y + j, center.z + k);
    }
}