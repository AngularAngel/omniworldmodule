/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.angle.omniworld.impl.space;

import lombok.Getter;
import net.angle.omniworld.api.space.CoordinateSystem;

/**
 *
 * @author angle
 */
public class BasicCoordinateSystem implements CoordinateSystem {
    private final @Getter int blockEdgeLengthOfChunk;
    private final @Getter float realEdgeLengthOfBlock;
    
    private final int voxelBitmask;
    private final int chunkBitmask;

    public BasicCoordinateSystem(int blockEdgeLengthOfChunk, float realEdgeLengthOfBlock) {
        this.blockEdgeLengthOfChunk = blockEdgeLengthOfChunk;
        this.realEdgeLengthOfBlock = realEdgeLengthOfBlock;
        assert blockEdgeLengthOfChunk % 2 == 0;
        voxelBitmask = blockEdgeLengthOfChunk - 1;        
        chunkBitmask = ~voxelBitmask;        
    }
    
    @Override
    public int getChunkCoordFromWorldCoord(int worldCoord) {
        return worldCoord & chunkBitmask;
    }
    
    @Override
    public int getVoxelCoordFromWorldCoord(int worldCoord) {
        return worldCoord & voxelBitmask;
    }
    
    @Override
    public int getWorldCoordFromRealCoord(float realCoord) {
        return (int) (realCoord / realEdgeLengthOfBlock);
    }
}