package net.angle.omniworld.impl.space;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.angle.omniworld.api.space.BlockFace;
import static net.angle.omniworld.impl.space.PrimaryDirections.DOWN;
import static net.angle.omniworld.impl.space.PrimaryDirections.EAST;
import static net.angle.omniworld.impl.space.PrimaryDirections.NORTH;
import static net.angle.omniworld.impl.space.PrimaryDirections.SOUTH;
import static net.angle.omniworld.impl.space.PrimaryDirections.UP;
import static net.angle.omniworld.impl.space.PrimaryDirections.WEST;

/**
 *
 * @author angle
 */
@AllArgsConstructor
public enum PrimaryBlockFaces implements BlockFace {
    TOP   ("Top"   , UP   , EAST , SOUTH),
    BOTTOM("Bottom", DOWN , EAST , NORTH),
    LEFT  ("Left"  , WEST , SOUTH, DOWN ),
    RIGHT ("Right" , EAST , NORTH, DOWN ),
    FRONT ("Front" , SOUTH, EAST , DOWN ),
    BACK  ("Back"  , NORTH, WEST , DOWN );
    
    public final @Getter String name;
    public final @Getter PrimaryDirections position, across, down;

    @Override
    public int getId() {
        return ordinal();
    }
    
    @Override
    public BlockFace getOpposite() {
        switch(this) {
            case TOP   : return BOTTOM;
            case BOTTOM: return TOP   ;
            case LEFT  : return RIGHT ;
            case RIGHT : return LEFT  ;
            case FRONT : return BACK  ;
            case BACK  : return FRONT ;
            default:
                throw new AssertionError(this.name());
        }
    }
}