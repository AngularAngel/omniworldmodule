/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.omniworld.impl.space;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.angle.omniworld.api.space.Alignment;
import net.angle.omniworld.api.space.Axis;
import net.angle.omniworld.api.space.BlockFace;
import net.angle.omniworld.api.space.Direction;

/**
 *
 * @author angle
 */
@AllArgsConstructor
public enum PrimaryDirections implements Direction {
    UP   ("Up"   , PrimaryAxes.Y, PrimaryAlignments.POSITIVE),
    DOWN ("Down" , PrimaryAxes.Y, PrimaryAlignments.NEGATIVE),
    NORTH("North", PrimaryAxes.Z, PrimaryAlignments.NEGATIVE),
    SOUTH("South", PrimaryAxes.Z, PrimaryAlignments.POSITIVE),
    EAST ("East" , PrimaryAxes.X, PrimaryAlignments.POSITIVE),
    WEST ("West" , PrimaryAxes.X, PrimaryAlignments.NEGATIVE);
    
    public final @Getter String name;
    public final @Getter Axis axis;
    public final @Getter Alignment alignment;
    
    @Override
    public PrimaryDirections getOpposite() {
        switch(this) {
            case UP   : return DOWN ;
            case DOWN : return UP   ;
            case NORTH: return SOUTH;
            case SOUTH: return NORTH;
            case EAST : return WEST ;
            case WEST : return EAST ;
            default:
                throw new AssertionError(this.name());
            
        }
    }
    
    public BlockFace getFace() {
        switch(this) {
            case UP   : return PrimaryBlockFaces.TOP   ;
            case DOWN : return PrimaryBlockFaces.BOTTOM;
            case NORTH: return PrimaryBlockFaces.BACK  ;
            case SOUTH: return PrimaryBlockFaces.FRONT ;
            case EAST : return PrimaryBlockFaces.RIGHT ;
            case WEST : return PrimaryBlockFaces.LEFT  ;
            default:
                throw new AssertionError(this.name());
        }
    }
}