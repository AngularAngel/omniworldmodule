package net.angle.omniworld.impl.space;

import com.samrj.devil.math.Vec3;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import net.angle.omniworld.api.space.CollisionContact;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
@ToString
public class BasicCollisionContact implements CollisionContact {
    private final @Getter Vec3 position, normal;
    private final @Getter float time;
}