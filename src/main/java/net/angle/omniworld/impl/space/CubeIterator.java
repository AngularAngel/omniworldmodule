package net.angle.omniworld.impl.space;

import com.samrj.devil.math.Vec3i;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.angle.omniworld.api.space.BoundedVoxelSpace;
import net.angle.omniworld.api.space.VoxelIterator;

/**
 *
 * @author angle
 */
@AllArgsConstructor
public class CubeIterator implements VoxelIterator {
    private @Getter @Setter int edgeDistance;
    private @Getter @Setter BoundedVoxelSpace.VoxelOperation operation;
    
    @Override
    public void iterate(Vec3i center) {
        iterate(center, operation);
    }
    
    @Override
    public void iterate(Vec3i center, BoundedVoxelSpace.VoxelOperation operation) {
        for (int i = -edgeDistance; i <= edgeDistance; i++)
            for (int j = -edgeDistance; j <= edgeDistance; j++)
                for (int k = -edgeDistance; k <= edgeDistance; k++)
                    operation.operate(center.x + i, center.y + j, center.z + k);
    }
}