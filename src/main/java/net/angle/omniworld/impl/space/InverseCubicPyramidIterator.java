package net.angle.omniworld.impl.space;

import com.samrj.devil.math.Vec3i;
import net.angle.omniworld.api.space.BoundedVoxelSpace;

/**
 *
 * @author angle
 */
public class InverseCubicPyramidIterator extends CubeIterator {

    public InverseCubicPyramidIterator(int edgeDistance, BoundedVoxelSpace.VoxelOperation operation) {
        super(edgeDistance, operation);
    }
    
    public int getCurEdgeDistance(int y) {
        return getEdgeDistance() + Math.max(y, 0);
    }
    
    @Override
    public void iterate(Vec3i center) {
        for (int y = -getEdgeDistance(); y <= getEdgeDistance(); y++)
            for (int x = -getCurEdgeDistance(y); x <= getCurEdgeDistance(y); x++)
                for (int z = -getCurEdgeDistance(y); z <= getCurEdgeDistance(y); z++)
                    getOperation().operate(center.x + x, center.y + y, center.z + z);
    }
}