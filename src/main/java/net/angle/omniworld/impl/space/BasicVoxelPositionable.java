/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.omniworld.impl.space;

import com.samrj.devil.math.Vec3i;
import lombok.EqualsAndHashCode;
import net.angle.omniworld.api.space.VoxelPositionable;

/**
 *
 * @author angle
 */
@EqualsAndHashCode(callSuper = true)
public class BasicVoxelPositionable extends Vec3i implements VoxelPositionable {
    
    public BasicVoxelPositionable(int x, int y, int z) {
        super(x, y, z);
    }
    
    public BasicVoxelPositionable(Vec3i vec) {
        super(vec);
    }
    
    @Override
    public Vec3i getCoordinates() {
        return new Vec3i(this);
    }
}