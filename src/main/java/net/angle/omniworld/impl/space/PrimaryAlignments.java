package net.angle.omniworld.impl.space;

import com.samrj.devil.math.Vec3i;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.angle.omniworld.api.space.Alignment;

/**
 *
 * @author angle
 */
@AllArgsConstructor
public enum PrimaryAlignments implements Alignment {
    POSITIVE("Positive", 1 ),
    NEGATIVE("Negative", -1);

    public final @Getter String name;
    public final @Getter int step;

    @Override
    public Alignment getOpposite() {
        switch(this) {
            case POSITIVE: return NEGATIVE;
            case NEGATIVE: return POSITIVE;
            default:
                throw new AssertionError(this.name());
        }
    }

    @Override
    public boolean continueStepping(int value, int limit) {
        switch(this) {
            case POSITIVE: return value < limit;
            case NEGATIVE: return value > 0;
            default:
                throw new AssertionError(this.name());
        }
    }
}