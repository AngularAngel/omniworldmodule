package net.angle.omniworld.impl.space;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author angle
 */
public class BasicCoordinateSystemTest {

    /**
     * Test of getChunkCoordFromWorldCoord method, of class BasicCoordinateSystem.
     */
    @Test
    public void testGetChunkCoordFromWorldCoord() {
        BasicCoordinateSystem instance = new BasicCoordinateSystem(16, 0.5f);
        assertEquals(0, instance.getChunkCoordFromWorldCoord(0));
        assertEquals(16, instance.getChunkCoordFromWorldCoord(16));
        assertEquals(16, instance.getChunkCoordFromWorldCoord(20));
        assertEquals(32, instance.getChunkCoordFromWorldCoord(32));
        assertEquals(32, instance.getChunkCoordFromWorldCoord(38));
    }

    /**
     * Test of getVoxelCoordFromWorldCoord method, of class BasicCoordinateSystem.
     */
    @Test
    public void testGetVoxelCoordFromWorldCoord() {
        BasicCoordinateSystem instance = new BasicCoordinateSystem(16, 0.5f);
        assertEquals(1, instance.getVoxelCoordFromWorldCoord(1));
        assertEquals(2, instance.getVoxelCoordFromWorldCoord(2));
        assertEquals(0, instance.getVoxelCoordFromWorldCoord(16));
    }

    /**
     * Test of getWorldCoordFromRealCoord method, of class BasicCoordinateSystem.
     */
    @Test
    public void testGetWorldCoordFromRealCoord() {
        BasicCoordinateSystem instance = new BasicCoordinateSystem(16, 0.5f);
        assertEquals(1, instance.getWorldCoordFromRealCoord(0.5f));
        assertEquals(2, instance.getWorldCoordFromRealCoord(1f));
        assertEquals(3, instance.getWorldCoordFromRealCoord(1.5f));
    }

    /**
     * Test of getBlockEdgeLengthOfChunk method, of class BasicCoordinateSystem.
     */
    @Test
    public void testGetBlockEdgeLengthOfChunk() {
        BasicCoordinateSystem instance = new BasicCoordinateSystem(16, 0.5f);
        assertEquals(16, instance.getBlockEdgeLengthOfChunk());
    }

    /**
     * Test of getRealEdgeLengthOfBlock method, of class BasicCoordinateSystem.
     */
    @Test
    public void testGetRealEdgeLengthOfBlock() {
        BasicCoordinateSystem instance = new BasicCoordinateSystem(16, 0.5f);
        assertEquals(0.5f, instance.getRealEdgeLengthOfBlock(), 0.0);
    }
}