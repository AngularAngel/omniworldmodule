#version 140

uniform float u_z_far;

in vec4 v_view_pos;

out vec3 out_color; //The color of a pixel/fragment.

void main() {
    out_color = vec3(1, 1, 1);

    gl_FragDepth = -v_view_pos.z/u_z_far;
}